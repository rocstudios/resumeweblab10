var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');


// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('resume/resumeViewById', {'result': result[0]});
           }
        });
    }
});

// Return the add a new resume form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getByAccountId(req.query.account_id, function(err,resume1) {
        resume_dal.edit2( function(err,resume2) {

            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeAdd', {result1: resume1[0], result2: resume2[0]});
            }
        });
    });
});

// Return the add a new resume form with select user
router.get('/selectuser', function(req, res) {
    resume_dal.getAccounts(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.resume_name == "") {
        res.send('Resume name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.query, function(err,result4){
            resume_dal.save_resume_companies(req.query, function(err,result1){
                resume_dal.save_resume_skills(req.query, function(err,result2){
                    resume_dal.save_resume_schools(req.query, function(err,result3){
                        if (err) {
                            console.log(err)
                            res.send(err);
                        }
                        else {
                            //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                            res.send('success');
                        }
                    });
                });
            });
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.account_id == null) {
       res.send('A account id is required');
   }
   else {
       resume_dal.getById(req.query.account_id, function(err, resume){
           resume_dal.getAll(function(err, resume) {
               res.render('resume/resumeUpdate', {result1: resume[0]});
           });
       });
   }

});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
       res.redirect(302, '/resume/all');
    });
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
         resume_dal.delete(req.query.resume_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/resume/all');
             }
         });
    }
});

module.exports = router;
