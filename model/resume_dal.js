var mysql  = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select * from resume

 */

exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.* FROM resume_ r join account a on a.account_id = r.account_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getAccounts = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'call company_school_skill_getinfo_by_resume_id(?)'
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getByAccountId = function(account_id, callback) {
    var query = 'select * from account where account_id = ?;';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

/*exports.getResumeTables = function(company_name, school_name, skill_name, callback) {
    var query = 'select company_id, school_name, skill_name from resume_ r' +
        'join resume_company rc on rc.'
        'where account_id = ?;';
    var queryData = [company_name, school_name, skill_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};*/

exports.insert = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO resume_ (account_id, resume_name) VALUES (?,?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.save_resume_schools = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?,?)';

    var queryData = [params.resume_id, params.school_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.save_resume_companies = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?,?)';

    var queryData = [params.resume_id, params.company_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.save_resume_skills = function(params, callback) {

    // FIRST INSERT THE RESUME
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES (?,?)';

    var queryData = [params.resume_id, params.skill_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume_ WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE resume_ SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS resume_getinfo;

     DELIMITER //
     CREATE PROCEDURE resume_getinfo (resume_id int)
     BEGIN

     SELECT * FROM resume WHERE resume_id = _resume_id;


     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL resume_getinfo (4);

 */



exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*

 drop procedure if exists company_school_skill_getinfo;
 DELIMITER //
 create procedure company_school_skill_getinfo_by_resume_id(_resume_id int)
 begin
 SELECT r.*, a.*, school_name, skill_name, company_name FROM resume_ r
 join account a on a.account_id = r.account_id
 join resume_school r_s on r_s.resume_id = _resume_id
 join school s on r_s.school_id = s.school_id
 join resume_company r_c on r_c.resume_id = _resume_id
 join company c on r_c.company_id = c.company_id
 join resume_skill r_k on r_k.resume_id = _resume_id
 join skill k on r_k.skill_id = k.skill_id;

 end; //

 call company_school_skill_getinfo();
 */

exports.edit2 = function(callback) {
    var query = 'CALL company_school_skill_getinfo()';
    //var queryData = [account_id];

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};